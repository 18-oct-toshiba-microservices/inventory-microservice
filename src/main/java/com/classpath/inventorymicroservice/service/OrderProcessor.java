package com.classpath.inventorymicroservice.service;

import com.classpath.inventorymicroservice.model.Order;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class OrderProcessor {

    private final KafkaTemplate kafkaTemplate;

    @KafkaListener(topics = {"orders-topic-new"}, groupId = "inventory-microservice")
    public void processOrder(String order) throws JsonProcessingException {
        log.info("Processing the order inside the inventory :: {}", order);
        ObjectMapper mapper = new ObjectMapper();
        Order orderToBeProcessed = mapper.readValue(order, Order.class);
        log.info("Order Id : {}", orderToBeProcessed.getOrderId());
        this.kafkaTemplate.send("inventory-processed", order);
    }

}